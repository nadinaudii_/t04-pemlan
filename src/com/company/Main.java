package com.company;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            //edit here
            try {
                System.out.print("Masukkan pembilang: ");
                int pembilang = input.nextInt(); //user memasukkan pembilang
                System.out.print("Masukkan penyebut: ");
                int penyebut = input.nextInt(); //user memasukkan penyebut

                int hasil = pembagian (pembilang, penyebut) ; // dilakukan pengecekan bilangan melalui operasi pembagian
                System.out.println("Input yang dimasukkan valid!!");
                System.out.println("Hasil dari pembagian: " + hasil);

                validInput = true; //untuk mengubah nilai menjadi true dan keluar dari loop
            } catch (InputMismatchException e) { //melakukan pengecekan terhadap input, apakah ada yang tidak sesuai dengan tipe data
                System.out.println("Input harus berupa bilangan bulat. Silakan input ulang.");
                input.nextLine();
            } catch (ArithmeticException e) {//melakukan pengecekan apakah penyebut dalam perhitungan pembagian bernilai nol
                System.out.println("Penyebut tidak boleh nol. Silakan input ulang.");
                input.nextLine();
            }
        } while (!validInput);
        input.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        //melakukan pembagian bilangan bulat dan melempar exception jika penyebut bernilai nol
        //add exception apabila penyebut bernilai 0
        if (penyebut == 0) {
             throw new ArithmeticException("Penyebut tidak boleh nol.");
        }
        return pembilang / penyebut;
    }
}
